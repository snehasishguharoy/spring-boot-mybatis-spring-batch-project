package com.example.demo.spring.batch.cnfg;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="spring.batch")
public class ConfigData {

	
	private int chunksize;
	private int insertReadCount;
	private int updateReadCount;
	private int skipCount;

	public int getChunksize() {
		return chunksize;
	}

	public void setChunksize(int chunksize) {
		this.chunksize = chunksize;
	}

	public int getInsertReadCount() {
		return insertReadCount;
	}

	public void setInsertReadCount(int insertReadCount) {
		this.insertReadCount = insertReadCount;
	}

	public int getUpdateReadCount() {
		return updateReadCount;
	}

	public void setUpdateReadCount(int updateReadCount) {
		this.updateReadCount = updateReadCount;
	}

	public int getSkipCount() {
		return skipCount;
	}

	public void setSkipCount(int skipCount) {
		this.skipCount = skipCount;
	}
	
}
