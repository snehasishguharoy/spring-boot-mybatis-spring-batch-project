package com.example.demo.spring.batch.mapper;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.spring.batch.domain.EmpPhDomain;
@Component
public interface EmpMapper {
	

	List<EmpPhDomain> fndEmpPhById(String emplId);
	void insPhEmp(EmpPhDomain domain);
	int delEmpPh(EmpPhDomain domain);
	int updtEmpPh(EmpPhDomain domain);

}
