package com.example.demo.spring.batch.cnfg;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.spring.batch.domain.EmpPhDomain;
import com.example.demo.spring.batch.listener.JobCompletionNotificationListener;
import com.example.demo.spring.batch.processor.EmployeeItemProcessor;
import com.example.demo.spring.batch.skip.FileVerificationSkipper;
import com.example.demo.spring.batch.tasklet.WriterTasklet;
import com.example.demo.spring.batch.wrtr.CustomWriter;

@Configuration
@EnableBatchProcessing
@ComponentScan(basePackages = "com.example.demo.spring.batch")
public class BatchCnfg {
	@Autowired
	private JobBuilderFactory builderFactory;
	@Autowired
	private StepBuilderFactory factory;

	@Autowired
	private WriterTasklet writerTasklet;

	@Autowired
	private SqlSessionFactory sqlSessionFactory;
	@Autowired
	private FileVerificationSkipper fileVerificationSkipper;

	@Autowired
	private CustomWriter customWriter;

	@Autowired
	private ConfigData configData;

	@Bean
	public FlatFileItemReader<EmpPhDomain> reader() {
		FlatFileItemReader<EmpPhDomain> reader = new FlatFileItemReader<EmpPhDomain>();
		reader.setResource(new ClassPathResource("Employee.csv"));
		reader.setLinesToSkip(1);
		reader.setSaveState(true);
		reader.setLineMapper(new DefaultLineMapper<EmpPhDomain>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer() {
					{
						setNames(new String[] { "empId", "empPhNb", "effDt", "phTypCd", "usrId", "empNm" });
					}
				});
				setFieldSetMapper(new BeanWrapperFieldSetMapper<EmpPhDomain>() {
					{
						setTargetType(EmpPhDomain.class);
					}
				});
			}
		});
reader.setCurrentItemCount(configData.getInsertReadCount());
	
		return reader;
	}

	@Bean
	public FlatFileItemReader<EmpPhDomain> reader3() {
		FlatFileItemReader<EmpPhDomain> reader = new FlatFileItemReader<EmpPhDomain>();
		reader.setResource(new ClassPathResource("Employee1.csv"));
		reader.setLinesToSkip(1);
		reader.setSaveState(true);
		// reader.setCurrentItemCount(1);
		reader.setLineMapper(new DefaultLineMapper<EmpPhDomain>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer() {
					{
						setNames(new String[] { "empId", "empPhNb", "newEmpPhNb", "effDt", "newEffDt", "phTypCd",
								"newPhTypCd", "usrId", "empNm" });
					}
				});
				setFieldSetMapper(new BeanWrapperFieldSetMapper<EmpPhDomain>() {
					{
						setTargetType(EmpPhDomain.class);
					}
				});
			}
		});
		reader.setCurrentItemCount(configData.getUpdateReadCount());

		return reader;
	}

	@Bean
	public EmployeeItemProcessor processor() {
		return new EmployeeItemProcessor();
	}

	@Bean
	public Job importUserJob(JobCompletionNotificationListener listener) {
		return builderFactory.get("insertJob").incrementer(new RunIdIncrementer()).listener(listener).flow(step1())
				// .next(taskletStep())
				// .flow(step3())
				 .next(step2())
				 //.next(step2())
				.end().build();

	}

	@Bean
	public Step step1() {
		System.out.println(configData.getChunksize());
		return factory.get("step1").<EmpPhDomain, EmpPhDomain>chunk(configData.getChunksize())
				.reader(reader())
				.processor(processor()).writer(customWriter).faultTolerant().skipPolicy(fileVerificationSkipper)
				.build();
	}

	@Bean
	public Step step3() {
		return factory.get("step3").<EmpPhDomain, EmpPhDomain>chunk(configData.getChunksize()).reader(reader3())
				.writer(writer2()).build();

	}

	@Bean
	public Step step2() {
		return factory.get("step2").<EmpPhDomain, EmpPhDomain>chunk(configData.getChunksize()).reader(reader3())
				.writer(writer3()).build();

	}

	@Bean
	public MyBatisBatchItemWriter<EmpPhDomain> writer() {
		MyBatisBatchItemWriter<EmpPhDomain> writer = new MyBatisBatchItemWriter<EmpPhDomain>();
		writer.setSqlSessionFactory(sqlSessionFactory);
		writer.setStatementId("com.example.demo.spring.batch.mapper.EmpMapper.insPhEmp");
		return writer;
	}

	@Bean
	public MyBatisBatchItemWriter<EmpPhDomain> writer3() {
		MyBatisBatchItemWriter<EmpPhDomain> writer = new MyBatisBatchItemWriter<EmpPhDomain>();
		writer.setSqlSessionFactory(sqlSessionFactory);
		// writer.setStatementId("com.example.demo.spring.batch.mapper.EmpMapper.updateEmployeeStoredProcedure");
		writer.setStatementId("com.example.demo.spring.batch.mapper.EmpMapper.updtEmpPh");
		return writer;
	}

	@Bean
	public MyBatisBatchItemWriter<EmpPhDomain> writer2() {
		MyBatisBatchItemWriter<EmpPhDomain> writer = new MyBatisBatchItemWriter<EmpPhDomain>();
		writer.setSqlSessionFactory(sqlSessionFactory);
		// writer.setStatementId("com.example.demo.spring.batch.mapper.EmpMapper.deleteEmployeeStoredProcedure");
		writer.setStatementId("com.example.demo.spring.batch.mapper.EmpMapper.delEmpPh");
		return writer;
	}

}
