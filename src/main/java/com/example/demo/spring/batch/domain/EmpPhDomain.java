package com.example.demo.spring.batch.domain;

import java.io.Serializable;

public class EmpPhDomain implements Serializable {
	private static final long serialVersionUID = 1L;
	private String empId;
	private String empPhNb;
	private String newEmpPhNb;
	private String effDt;
	private String newEffDt;
	private String phTypCd;
	private String newPhTypCd;
	private String usrId;
	private String empNm;
	public String getEmpId() {
		return empId;
	}
	@Override
	public String toString() {
		return "EmpPhDomain [empId=" + empId + ", empPhNb=" + empPhNb + ", newEmpPhNb=" + newEmpPhNb + ", effDt="
				+ effDt + ", newEffDt=" + newEffDt + ", phTypCd=" + phTypCd + ", newPhTypCd=" + newPhTypCd + ", usrId="
				+ usrId + ", empNm=" + empNm + "]";
	}
	
	
	public EmpPhDomain(String empId, String empPhNb, String newEmpPhNb, String effDt, String newEffDt, String phTypCd,
			String newPhTypCd, String usrId, String empNm) {
		super();
		this.empId = empId;
		this.empPhNb = empPhNb;
		this.newEmpPhNb = newEmpPhNb;
		this.effDt = effDt;
		this.newEffDt = newEffDt;
		this.phTypCd = phTypCd;
		this.newPhTypCd = newPhTypCd;
		this.usrId = usrId;
		this.empNm = empNm;
	}
	public EmpPhDomain() {
		super();
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getEmpPhNb() {
		return empPhNb;
	}
	public void setEmpPhNb(String empPhNb) {
		this.empPhNb = empPhNb;
	}
	public String getNewEmpPhNb() {
		return newEmpPhNb;
	}
	public void setNewEmpPhNb(String newEmpPhNb) {
		this.newEmpPhNb = newEmpPhNb;
	}
	public String getEffDt() {
		return effDt;
	}
	public void setEffDt(String effDt) {
		this.effDt = effDt;
	}
	public String getNewEffDt() {
		return newEffDt;
	}
	public void setNewEffDt(String newEffDt) {
		this.newEffDt = newEffDt;
	}
	public String getPhTypCd() {
		return phTypCd;
	}
	public void setPhTypCd(String phTypCd) {
		this.phTypCd = phTypCd;
	}
	public String getNewPhTypCd() {
		return newPhTypCd;
	}
	public void setNewPhTypCd(String newPhTypCd) {
		this.newPhTypCd = newPhTypCd;
	}
	public String getUsrId() {
		return usrId;
	}
	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}
	public String getEmpNm() {
		return empNm;
	}
	public void setEmpNm(String empNm) {
		this.empNm = empNm;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
}
