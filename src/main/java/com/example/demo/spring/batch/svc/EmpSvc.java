///**
// * 
// */
//package com.example.demo.spring.batch.svc;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Date;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestHeader;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.example.demo.spring.batch.dao.EmpDaoImpl;
//import com.example.demo.spring.batch.domain.EmpDomain;
//
//@RestController
//public class EmpSvc {
//	@Autowired
//	private EmpDaoImpl daoImpl;
//	public static List<EmpDomain> getEmps=new ArrayList<>();
//
//	static{
//		getEmps=Arrays.asList(new EmpDomain("E101","RAHUL","ROY","KOLKATA",new Date(),"1"),new EmpDomain("E102","PRAMIT","DAS","MUMBAI",new Date(),"5"));
//	}
//
//	@PostMapping(value = "/employees", produces = { "application/json" }, consumes = { "application/json" })
//	public List<EmpDomain> getEmps(@RequestHeader("transactionId") String transactionId,
//			@RequestHeader("applicationName") String applicationName, @RequestBody String empId) {
//		return getEmps;
//
//	}
//
//}
