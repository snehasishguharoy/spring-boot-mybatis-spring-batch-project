package com.example.demo.spring.batch.listener;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.spring.batch.domain.EmpPhDomain;
import com.example.demo.spring.batch.mapper.EmpMapper;



@Component
public class JobCompletionNotificationListener extends JobExecutionListenerSupport  {
	@Autowired
	private SqlSessionFactory sqlSessionFactory;
	
	private static final Logger log = LoggerFactory.getLogger(JobCompletionNotificationListener.class);
	@Override
	public void afterJob(JobExecution jobExecution) {
		if(jobExecution.getStatus() == BatchStatus.COMPLETED) {
			log.info("!!! JOB FINISHED! Time to verify the results");
			SqlSession sqlSession=sqlSessionFactory.openSession();
			EmpMapper empMapperIfc=sqlSession.getMapper(EmpMapper.class);
			 List<EmpPhDomain> empDomains=empMapperIfc.fndEmpPhById("empId");


			for (EmpPhDomain emp : empDomains) {
				log.info("Found <" + emp + "> in the database.");
			}

		}
	}

}
