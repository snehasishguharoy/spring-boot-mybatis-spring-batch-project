package com.example.demo.spring.batch.wrtr;

import java.util.List;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.spring.batch.domain.EmpPhDomain;
import com.example.demo.spring.batch.mapper.EmpMapper;
@Component
public class CustomWriter implements ItemWriter<EmpPhDomain> {
	@Autowired
	private SqlSessionFactory sqlSessionFactory;

	@Override
	public void write(List<? extends EmpPhDomain> items) throws Exception {
		for(EmpPhDomain domain:items){
			
			EmpMapper empMapper=sqlSessionFactory.openSession().getMapper(EmpMapper.class);
			empMapper.insPhEmp(domain);
			
		}
		
	} 

}
