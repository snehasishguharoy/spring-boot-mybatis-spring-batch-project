package com.example.demo.spring.batch.skip;

import java.io.FileNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;

import com.example.demo.spring.batch.cnfg.ConfigData;

@Component
public class FileVerificationSkipper implements SkipPolicy {
	  private static final Logger logger = LoggerFactory.getLogger("badRecordLogger");
	  @Autowired
	  private ConfigData configData;

	@Override
	public boolean shouldSkip(Throwable exception, int skipCount) throws SkipLimitExceededException {

		if (exception instanceof FileNotFoundException) {

			return false;
		}

//		} else if (exception instanceof FlatFileParseException && skipCount <=0) {
//
//			FlatFileParseException ffpe = (FlatFileParseException) exception;
//			System.out.println(skipCount);
//
//			StringBuilder errorMessage = new StringBuilder();
//
//			errorMessage.append("An error occured while processing the " + ffpe.getLineNumber()
//
//					+ " line of the file. Below was the faulty " + "input.\n");
//
//			errorMessage.append(ffpe.getInput() + "\n");
//
//			logger.error("{}", errorMessage.toString());
//
//			return true;
//
//	}
		else if (exception instanceof DuplicateKeyException && skipCount <= configData.getSkipCount()) {

			org.springframework.dao.DuplicateKeyException ffpe = (org.springframework.dao.DuplicateKeyException) exception;

			StringBuilder errorMessage = new StringBuilder();


			return true;

		}
		else if (exception instanceof org.springframework.dao.DataIntegrityViolationException && skipCount <= configData.getSkipCount()) {

			org.springframework.dao.DataIntegrityViolationException ffpe = (org.springframework.dao.DataIntegrityViolationException) exception;

			StringBuilder errorMessage = new StringBuilder();

			 errorMessage.append(ffpe.getMessage() + "\n");

			return true;

		}
		else if (exception instanceof java.sql.SQLIntegrityConstraintViolationException && skipCount <= configData.getSkipCount()) {

			java.sql.SQLIntegrityConstraintViolationException ffpe = (java.sql.SQLIntegrityConstraintViolationException) exception;

			StringBuilder errorMessage = new StringBuilder();

			 errorMessage.append(ffpe.getMessage() + "\n");

			return true;

		}
		
		else if (exception instanceof  org.apache.ibatis.exceptions.PersistenceException && skipCount <= configData.getSkipCount()) {

			 org.apache.ibatis.exceptions.PersistenceException ffpe = (org.apache.ibatis.exceptions.PersistenceException) exception;

			StringBuilder errorMessage = new StringBuilder();

			 errorMessage.append(ffpe.getMessage() + "\n");

			return true;

		}
		
		
		else {

			return false;

		}


}
}
