package com.example.demo.spring.batch;

import javax.naming.NamingException;

import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.explore.support.MapJobExplorerFactoryBean;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import com.example.demo.spring.batch.cnfg.CmnCnfg;

@SpringBootApplication
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})

public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
//	@Bean
//	public DataSourceTransactionManager dataSourceTransactionManager() throws IllegalArgumentException, NamingException{
//		DataSourceTransactionManager dataSourceTransactionManager=new DataSourceTransactionManager();
//		CmnCnfg cmnCnfg=new CmnCnfg();
//		dataSourceTransactionManager.setDataSource(cmnCnfg.cssDataSource());
//		
//		return dataSourceTransactionManager;
//	}
//	
//	@Bean
//	public PlatformTransactionManager transactionManager() throws IllegalArgumentException, NamingException{
//		return dataSourceTransactionManager();
//	}
//	@Bean
//	public JobExplorer jobExplorer() throws Exception{
//		MapJobExplorerFactoryBean explorerFactoryBean=new MapJobExplorerFactoryBean();
//		explorerFactoryBean.setRepositoryFactory(mapJobRepositoryFactoryBean());
//		explorerFactoryBean.afterPropertiesSet();
//		return explorerFactoryBean.getObject();
//	}
//	@Bean
//	public MapJobRepositoryFactoryBean mapJobRepositoryFactoryBean() throws IllegalArgumentException, NamingException{
//		MapJobRepositoryFactoryBean mapJobRepositoryFactoryBean=new MapJobRepositoryFactoryBean();
//		mapJobRepositoryFactoryBean.setTransactionManager(transactionManager());
//		return mapJobRepositoryFactoryBean;
//	}
//	
//	@Bean 
//	public JobRepository jobRepository() throws Exception{
//		return mapJobRepositoryFactoryBean().getObject();
//	}
//	
//	@Bean
//	public JobLauncher jobLauncher() throws Exception{
//		SimpleJobLauncher jobLauncher=new SimpleJobLauncher();
//		jobLauncher.setJobRepository(jobRepository());
//		return jobLauncher;
//	}
	
}
