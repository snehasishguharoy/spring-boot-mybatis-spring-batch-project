package com.example.demo.spring.batch.cnfg;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.MappedTypes;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.apache.tomcat.util.descriptor.web.ContextResource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jndi.JndiObjectFactoryBean;

import com.example.demo.spring.batch.domain.EmpPhDomain;

@Configuration
@MappedTypes(EmpPhDomain.class)
public class CmnCnfg {
	

	
	@Bean
	@Profile(value={"development","si"})
	public DataSource cssDataSource() throws IllegalArgumentException, NamingException {
		JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
		bean.setJndiName("java:comp/env/jdbc/BatchDataSource");
		bean.setProxyInterface(DataSource.class);
		bean.setLookupOnStartup(false);
		bean.afterPropertiesSet();
		System.out.println(bean.getObject());
		return (DataSource) bean.getObject();
	}

	@Bean
	@Profile(value={"test"})
	public DataSource testDataSource() throws IllegalArgumentException, NamingException {
		BasicDataSource basicDataSource=new BasicDataSource();
		basicDataSource.setUrl("jdbc:oracle:thin:@localhost:1521:xe");
		basicDataSource.setUsername("SYSTEM");
		basicDataSource.setPassword("ADMIN");
		basicDataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
//		JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
//		bean.setJndiName("java:comp/env/jdbc/BatchDataSource");
//		bean.setProxyInterface(DataSource.class);
//		bean.setLookupOnStartup(false);
//		bean.afterPropertiesSet();
//		System.out.println(bean.getObject());
		return basicDataSource;
	}
	
	@Bean
	@Profile(value={"test"})
	public SqlSessionFactory testSqlSessionFactoryBean() throws Exception {
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(testDataSource());
		sqlSessionFactoryBean.setTypeAliasesPackage("com.example.demo.spring.batch.domain");
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath:/EmpMapper.xml"));
		System.out.println(sqlSessionFactoryBean.getObject());
		return sqlSessionFactoryBean.getObject();
	}



	@Bean
	@Profile(value={"development","si"})
	public SqlSessionFactory cssSqlSessionFactoryBean() throws Exception {
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(cssDataSource());
		sqlSessionFactoryBean.setTypeAliasesPackage("com.example.demo.spring.batch.domain");
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath:/EmpMapper.xml"));
		System.out.println(sqlSessionFactoryBean.getObject());
		return sqlSessionFactoryBean.getObject();
	}

	@Bean
	@Profile(value={"test","si","development"})
	public TomcatEmbeddedServletContainerFactory tomcatFactory() {
		return new TomcatEmbeddedServletContainerFactory() {

			@Override
			protected TomcatEmbeddedServletContainer getTomcatEmbeddedServletContainer(Tomcat tomcat) {
				tomcat.enableNaming();
				return super.getTomcatEmbeddedServletContainer(tomcat);
			}

			@Override
			protected void postProcessContext(Context context) {
				ContextResource resource = new ContextResource();
				resource.setName("jdbc/BatchDataSource");
				resource.setType(DataSource.class.getName());
				resource.setProperty("driverClassName", "oracle.jdbc.driver.OracleDriver");
				resource.setProperty("url", "jdbc:oracle:thin:@localhost:1521:xe");
				resource.setProperty("username", "SYSTEM");
				resource.setProperty("password", "ADMIN");

				context.getNamingResources().addResource(resource);


			}
		};
	}
}
