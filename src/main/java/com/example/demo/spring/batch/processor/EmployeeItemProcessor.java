package com.example.demo.spring.batch.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.example.demo.spring.batch.domain.EmpPhDomain;

public class EmployeeItemProcessor implements ItemProcessor<EmpPhDomain, EmpPhDomain> {

	private static final Logger log = LoggerFactory.getLogger(EmployeeItemProcessor.class);


	@Override
	public EmpPhDomain process(EmpPhDomain item) throws Exception {
		final String firstName =item.getEmpNm().toUpperCase();
		
		final EmpPhDomain transformed = new EmpPhDomain(item.getEmpId(),item.getEmpPhNb(),null,item.getEffDt(),null,item.getPhTypCd(),null,item.getUsrId(),firstName);
		log.info("Converting (" + item + ") into (" + transformed + ")");

		return transformed;
	}

}
