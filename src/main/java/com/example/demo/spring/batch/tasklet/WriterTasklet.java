package com.example.demo.spring.batch.tasklet;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.spring.batch.domain.EmpPhDomain;
@Component
public class WriterTasklet implements Tasklet {
	@Autowired
	private SqlSessionFactory sqlSessionFactory;
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		MyBatisBatchItemWriter<EmpPhDomain> writer = new MyBatisBatchItemWriter<EmpPhDomain>();
		writer.setSqlSessionFactory(sqlSessionFactory);
	writer.setStatementId("com.example.demo.spring.batch.mapper.EmpMapper.insPhEmp");
		return null;
	}

}
