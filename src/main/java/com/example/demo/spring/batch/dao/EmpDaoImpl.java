//package com.example.demo.spring.batch.dao;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.apache.ibatis.session.SqlSession;
//import org.apache.ibatis.session.SqlSessionFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//
//import com.example.demo.spring.batch.domain.EmpDomain;
//import com.example.demo.spring.batch.mapper.EmpMapper;
//
//@Repository
//public class EmpDaoImpl implements EmpDaoIfc{
//	
//	@Autowired
//	private SqlSessionFactory sqlSessionFactory;
//	@Override
//	public List<EmpDomain> fndEmpById(String emplId) {
//		List<EmpDomain> empDomains=new ArrayList<>();
//		SqlSession sqlSession=sqlSessionFactory.openSession();
//		EmpMapper empMapperIfc=sqlSession.getMapper(EmpMapper.class);
//		return empMapperIfc.fndEmpById(emplId);
//	}
//
//
//
//}
