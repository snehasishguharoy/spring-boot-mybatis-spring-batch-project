package com.example.demo.spring.batch.mybatisspringbootspringbatchdemo;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jndi.JndiObjectFactoryBean;

@Configuration
@EnableBatchProcessing
public class TestConfig {
	
	@Bean
	public JobLauncherTestUtils jobLauncherTestUtils(){
		return new JobLauncherTestUtils();
	}
	@Bean
	@Profile(value={"test"})
	public DataSource testDataSource() throws IllegalArgumentException, NamingException {
		BasicDataSource basicDataSource=new BasicDataSource();
		basicDataSource.setUrl("jdbc:oracle:thin:@localhost:1521:xe");
		basicDataSource.setUsername("SYSTEM");
		basicDataSource.setPassword("ADMIN");
		basicDataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
//		JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
//		bean.setJndiName("java:comp/env/jdbc/BatchDataSource");
//		bean.setProxyInterface(DataSource.class);
//		bean.setLookupOnStartup(false);
//		bean.afterPropertiesSet();
//		System.out.println(bean.getObject());
		return basicDataSource;
	}
	
	@Bean
	@Profile(value={"test"})
	public SqlSessionFactory testSqlSessionFactoryBean() throws Exception {
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(testDataSource());
		sqlSessionFactoryBean.setTypeAliasesPackage("com.example.demo.spring.batch.domain");
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath:/EmpMapper.xml"));
		System.out.println(sqlSessionFactoryBean.getObject());
		return sqlSessionFactoryBean.getObject();
	}

	
	

}
