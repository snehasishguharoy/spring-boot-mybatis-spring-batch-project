package com.example.demo.spring.batch.mybatisspringbootspringbatchdemo;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.demo.spring.batch.cnfg.BatchCnfg;
import com.example.demo.spring.batch.cnfg.CmnCnfg;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={BatchCnfg.class,TestConfig.class})
@ActiveProfiles(value="test")
public class BatchCnfgTest {
	
	   	@Autowired
	    private JobLauncherTestUtils jobLauncherTestUtils;
	   	
	   	@Test
	   	@Ignore
	   	public void testJob() throws Exception{
	   		BatchStatus jobExecution = jobLauncherTestUtils.launchJob().getStatus();
	   		Assert.assertEquals("COMPLETED", jobExecution.getBatchStatus().name());
	   		
	   	}
	   	


}
