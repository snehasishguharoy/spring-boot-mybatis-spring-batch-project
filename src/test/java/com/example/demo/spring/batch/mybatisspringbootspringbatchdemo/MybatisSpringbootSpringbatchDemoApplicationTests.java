package com.example.demo.spring.batch.mybatisspringbootspringbatchdemo;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import com.example.demo.spring.batch.Application;
import com.example.demo.spring.batch.cnfg.BatchCnfg;
import com.example.demo.spring.batch.cnfg.CmnCnfg;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(value="test")
@SpringBootTest(classes={Application.class})
@ContextConfiguration(loader=AnnotationConfigContextLoader.class)
public class MybatisSpringbootSpringbatchDemoApplicationTests {

	@Test
	@Ignore
	public void contextLoads() {
	}

}
	